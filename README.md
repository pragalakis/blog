# blog

Blog SSG

Here is a picture of the theme
![screenshot-homepage](screenshot-homepage.jpg)
![screenshot-post](screenshot-post.jpg)

I know its all grey and sad but it's for simplicity's sake


## run it

- clone this repo
- Remove the `.git` folder
- `npm install`
- `npm run dev` 

## add content

Each post is a folder inside the blogposts folder.
In the markdown file (index.md) you can add the title, date, tags, decription, image and the text you like to post.

## build it

- `npm run build`
This does a production build of your site and outputs the built static files into the public directory.

To view the production site locally. Run: `npm run serve`


## What's inside?

Gatsbyjs (with a number of plugins) and react!


## Features 

- paginated view
- tags support
- excerpt and image
- rss feed
- sitemap
- search through duckduckgo

also
- no cloudflare
- no analytics
- no cookies
