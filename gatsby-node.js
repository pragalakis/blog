const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`);

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;

  return graphql(
    `
      {
        allMarkdownRemark(
          sort: { fields: [frontmatter___date], order: DESC }
          limit: 1000
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                title
                tags
              }
            }
          }
        }
      }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors;
    }

    const posts = result.data.allMarkdownRemark.edges;

    // Create blog posts pages.
    posts.forEach((post, i) => {
      const previous = i === posts.length - 1 ? null : posts[i + 1].node;
      const next = i === 0 ? null : posts[i - 1].node;

      createPage({
        path: post.node.fields.slug,
        component: path.resolve(`./src/templates/blog-post.js`),
        context: {
          slug: post.node.fields.slug,
          previous,
          next
        }
      });
    });

    // Create blog post list
    const postsPerPage = 4;
    const numPages = Math.ceil(posts.length / postsPerPage);

    Array.from({ length: numPages }).forEach((_, i) => {
      createPage({
        path: i === 0 ? `/` : `/${i + 1}`,
        component: path.resolve('./src/templates/index.js'),
        context: {
          limit: postsPerPage,
          skip: i * postsPerPage,
          numPages,
          currentPage: i + 1
        }
      });
    });

    // Create News post list
    const newsPosts = posts.filter(post =>
      post.node.frontmatter.tags.includes('news')
    );
    const numNewsPages = Math.ceil(newsPosts.length / postsPerPage);

    Array.from({ length: numNewsPages }).forEach((_, i) => {
      createPage({
        path: i === 0 ? `/news` : `/news/${i + 1}`,
        component: path.resolve('./src/templates/news.js'),
        context: {
          limit: postsPerPage,
          skip: i * postsPerPage,
          numNewsPages,
          currentPage: i + 1
        }
      });
    });

    // Create tag page
    let tags = [];
    // Iterate through each post, putting all found tags into `tags`
    posts.forEach(post => {
      if (post.node.frontmatter.tags) {
        tags = tags.concat(post.node.frontmatter.tags);
      }
    });

    // Eliminate duplicate tags
    tags = [...new Set(tags)];

    tags.forEach(tag => {
      createPage({
        path: `/tags/${tag}`,
        component: path.resolve('./src/templates/tags.js'),
        context: {
          tag
        }
      });
    });

    return null;
  });
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode });
    createNodeField({
      name: `slug`,
      node,
      value
    });
  }
};
