import React from 'react';
import { Link } from 'gatsby';
import Img from 'gatsby-image';

export default ({ posts }) => {
  return (
    <div className="article-list">
      {posts.map(({ node }) => {
        const { slug } = node.fields;
        const { title, date, image, description } = node.frontmatter;

        return (
          <article key={slug}>
            <h2 className="title">
              <Link to={slug}>{title}</Link>
            </h2>
            <div className="date">{date}</div>
            {image && (
              <Img
                fluid={image.childImageSharp.fluid}
                className="featured-image"
                alt="featured-image"
              />
            )}
            <p
              className="excerpt"
              dangerouslySetInnerHTML={{
                __html: description || node.excerpt
              }}
            />
          </article>
        );
      })}
    </div>
  );
};
