import React from 'react';
import '../css/footer.css';

export default () => {
  return (
    <footer>
      <section className="column">
        <h3>Blog</h3>
        <p>
          The content of the texts belongs to the original author / medium,
          where it is written.
        </p>
        <p>
          All the other content is under{' '}
          <a
            href="https://creativecommons.org/licenses/by-sa/4.0/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Creative Commons Attribution-ShareAlike
          </a>
        </p>
      </section>
      <section className="column">
        <h3>Support us</h3>
        <p>
          If you like what you hear, read and see in Blog, we need your help to
          continue.
        </p>
        <p>
          You can contribute financially to the effort through{' '}
          <a
            href="https://liberapay.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Liberapay
          </a>
          .
        </p>
      </section>
      <section className="column">
        <h3>Info</h3>
        <p>We don't store your data.</p>
        <p>
          We physically can't. We have nowhere to store it. We don't even have a
          server database to store it since this is a static website.
        </p>
      </section>
    </footer>
  );
};
