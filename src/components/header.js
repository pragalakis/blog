import React from 'react';
import { Link } from 'gatsby';
import Nav from './nav.js';
import '../css/header.css';

export default ({ title }) => {
  return (
    <header>
      <div className="logo">
        <Link className="header_title" to={`/`}>
          {title}
        </Link>
      </div>
      <div className="navigation">
        <Nav />
        <div className="social">
          <a
            className="twitter"
            href="https://twitter.com/"
            target="_blank"
            rel="noopener noreferrer"
            title="twitter"
          >
            <svg
              version="1.1"
              x="0px"
              y="0px"
              viewBox="0 0 30.77976 25.00209"
              width="30.77976"
              height="25.00209"
            >
              <path d="M 30.77976,2.95813 C 29.64689,3.46011 28.43134,3.80055 27.15418,3.95254 28.45824,3.17163 29.45648,1.93299 29.92959,0.4616 28.70634,1.18477 27.35611,1.70989 25.9174,1.99455 24.76532,0.76552 23.1266,0 21.30898,0 c -3.48707,0 -6.31443,2.82737 -6.31443,6.31252 0,0.49429 0.0558,0.97706 0.1635,1.43871 C 9.9111,7.48769 5.25847,4.97385 2.14453,1.15399 1.6002,2.08493 1.29054,3.16967 1.29054,4.32758 c 0,2.19075 1.11557,4.12374 2.80816,5.25464 C 3.0639,9.54762 2.09067,9.26296 1.23864,8.7898 v 0.0789 c 0,3.05816 2.17727,5.61047 5.06423,6.19136 -0.52894,0.14234 -1.0867,0.2212 -1.66372,0.2212 -0.40773,0 -0.80203,-0.0404 -1.18864,-0.11734 0.80395,2.51001 3.13511,4.33527 5.8971,4.38532 -2.15997,1.69258 -4.88348,2.69851 -7.84161,2.69851 -0.50968,0 -1.01171,-0.0308 -1.506,-0.0865 2.79468,1.79453 6.11251,2.84084 9.67844,2.84084 11.61525,0 17.96434,-9.62075 17.96434,-17.96434 l -0.0212,-0.81742 c 1.24054,-0.88482 2.31376,-1.99652 3.15815,-3.26211 z" />
            </svg>
          </a>
          <a
            className="ssb"
            href="https://www.scuttlebutt.nz/"
            target="_blank"
            rel="noopener noreferrer"
            title="scuttlebutt"
          >
            <svg
              version="1.0"
              width="21.506639pt"
              height="18.750004pt"
              viewBox="0 0 21.506639 18.750004"
            >
              <g
                transform="matrix(0.01937535,0,0,-0.01937535,-1.7437815,21.717761)"
                id="g49"
              >
                <path
                  d="m 350,1100 c -16,-16 -20,-33 -20,-82 0,-45 -6,-71 -19,-91 l -18,-28 -23,28 c -17,22 -21,37 -17,71 8,73 -50,116 -112,82 -22,-11 -51,-79 -51,-118 1,-55 81,-182 115,-182 7,0 18,-6 24,-13 8,-11 0,-18 -38,-36 C 101,691 90,677 90,606 c 0,-75 24,-162 53,-195 12,-13 42,-37 66,-52 24,-16 49,-39 55,-51 14,-30 147,-99 254,-134 102,-33 132,-29 238,28 51,27 79,38 82,30 2,-7 12,-12 23,-12 15,0 19,7 19,33 0,47 22,72 76,87 56,16 104,54 104,83 1,29 36,107 50,107 26,0 50,41 50,86 0,31 7,54 20,71 11,14 20,32 20,40 0,24 -47,63 -77,63 -15,0 -46,11 -68,25 -22,13 -46,25 -53,25 -8,0 -27,14 -42,30 -16,17 -38,30 -52,30 -18,0 -30,10 -46,39 -27,48 -72,82 -165,127 l -74,35 -64,-15 c -40,-10 -65,-12 -67,-6 -15,43 -106,56 -142,20 z m 355,-95 c 61,-34 88,-58 118,-107 23,-36 22,-38 -14,-38 -49,0 -95,-28 -138,-84 -28,-36 -41,-63 -41,-84 0,-38 -17,-41 -97,-17 -49,15 -50,16 -57,61 -9,71 -7,96 9,114 13,15 14,21 1,51 -14,33 -19,66 -17,101 1,12 14,19 49,24 26,4 56,10 67,15 26,10 48,4 120,-36 z M 927,824 c 16,-25 16,-26 -18,-52 -61,-46 -109,-102 -109,-126 0,-53 85,-115 160,-116 31,-1 32,-2 13,-10 -13,-5 -45,-12 -72,-16 -68,-9 -115,15 -149,77 -15,26 -39,55 -54,64 -40,23 -44,50 -13,94 44,64 68,81 130,91 32,5 60,12 63,15 12,12 33,3 49,-21 z m 87,-47 c 16,-12 16,-14 -3,-31 -28,-25 -26,-57 4,-86 19,-18 31,-21 57,-17 33,7 33,6 8,-8 -14,-7 -46,-19 -71,-25 -38,-9 -53,-9 -83,4 -44,18 -53,38 -35,74 16,29 84,102 97,102 5,0 16,-6 26,-13 z M 325,710 c 4,-6 -21,-41 -55,-78 -34,-37 -66,-74 -70,-82 -5,-9 0,-40 11,-75 10,-32 17,-60 16,-62 -2,-1 -15,9 -30,23 -32,30 -59,117 -55,176 3,42 4,44 68,75 67,33 104,41 115,23 z m 99,-49 c 4,-5 -4,-66 -16,-135 l -22,-127 22,-61 c 12,-34 20,-63 18,-65 -2,-2 -27,7 -56,22 -57,29 -68,47 -95,157 -16,63 -16,66 4,98 11,19 39,57 63,86 34,41 47,50 60,43 9,-5 19,-13 22,-18 z m 113,-83 c 6,-8 7,-35 2,-73 -4,-33 -8,-85 -8,-116 -1,-49 3,-60 26,-84 15,-15 47,-38 71,-52 38,-22 42,-27 27,-36 -23,-13 -99,-3 -129,16 -15,10 -33,43 -51,92 -25,68 -27,84 -22,159 4,45 10,97 13,114 l 6,32 28,-20 c 15,-10 31,-25 37,-32 z M 655,409 c 6,-88 10,-105 30,-125 13,-13 21,-24 16,-24 -15,0 -102,62 -111,80 -11,21 -13,106 -4,162 6,37 7,37 34,23 26,-14 28,-18 35,-116 z"
                  id="path47"
                />
              </g>
            </svg>
          </a>
          <a
            className="mastodon"
            href="https://mastodon.social/"
            target="_blank"
            rel="noopener noreferrer"
            title="mastodon"
          >
            <svg
              height="26.805609"
              version="1.1"
              viewBox="0 0 24.99999 26.805609"
              width="24.99999"
            >
              <g id="g463" transform="translate(-243.49994,-242.59657)">
                <path
                  d="m 268.499,251.39754 c 0,-5.81504 -3.81082,-7.51993 -3.81082,-7.51993 -3.739,-1.717 -13.66998,-1.69909 -17.37304,0 0,0 -3.81082,1.70495 -3.81082,7.51993 0,6.92172 -0.39485,15.51845 6.31746,17.29529 2.42285,0.63995 4.50475,0.7776 6.17982,0.68189 3.03908,-0.16748 4.74404,-1.08292 4.74404,-1.08292 l -0.10159,-2.20757 c 0,0 -2.17175,0.68213 -4.6125,0.60426 -2.41698,-0.0837 -4.96549,-0.26319 -5.36034,-3.23046 -0.0359,-0.26325 -0.0538,-0.53843 -0.0538,-0.83151 5.12098,1.2502 9.48802,0.54442 10.69061,0.40084 3.35613,-0.40084 6.28158,-2.47076 6.65248,-4.36129 0.58641,-2.97912 0.53849,-7.26853 0.53849,-7.26853 z m -4.49283,7.48998 h -2.78781 v -6.83193 c 0,-2.97326 -3.82897,-3.08695 -3.82897,0.41282 v 3.73906 h -2.76972 v -3.73912 c 0,-3.49977 -3.82879,-3.38608 -3.82879,-0.41283 v 6.83194 h -2.79373 c 0,-7.30448 -0.31106,-8.84807 1.10076,-10.46923 1.54946,-1.72898 4.77399,-1.8426 6.20976,0.36491 l 0.69399,1.16658 0.69393,-1.16658 c 1.44182,-2.21956 4.67228,-2.08191 6.20976,-0.36491 1.41793,1.6332 1.10082,3.17074 1.10082,10.46929 z"
                  id="path461"
                />
              </g>
            </svg>
          </a>
          <a
            className="dat"
            href="http://datproject.org/"
            target="_blank"
            rel="noopener noreferrer"
            title="DAT website"
          >
            <svg
              version="1.1"
              width="20.79579"
              height="24.047609"
              viewBox="0 0 20.79579 24.047609"
            >
              <g transform="translate(-289.56345,-292.37588)">
                <path
                  id="path835"
                  d="m 299.1786,315.97947 c -0.764,-0.44429 -3.11527,-1.80147 -6.29318,-3.63248 -0.93056,-0.53616 -2.05848,-1.18745 -2.50649,-1.4473 l -0.81456,-0.47247 -4.6e-4,-6.02573 -4.6e-4,-6.02574 0.81502,-0.47139 c 0.44826,-0.25926 1.64138,-0.94749 2.65139,-1.5294 1.01,-0.58191 2.43061,-1.40199 3.1569,-1.8224 0.72629,-0.42041 1.87256,-1.08215 2.54727,-1.47053 l 1.22673,-0.70615 0.42393,0.25087 c 0.23317,0.13797 0.81391,0.47413 1.29054,0.74702 0.47663,0.27289 1.75796,1.01143 2.8474,1.6412 1.08944,0.62976 2.52862,1.46076 3.19817,1.84665 0.66956,0.38589 1.53306,0.88527 1.91891,1.10972 l 0.70153,0.4081 0.009,6.02389 0.009,6.02389 -1.14339,0.65785 c -0.62886,0.36181 -1.79333,1.03149 -2.58771,1.48817 -0.79439,0.45668 -1.66718,0.96138 -1.93954,1.12155 -0.50104,0.29466 -4.27615,2.46795 -4.5806,2.637 l -0.16507,0.0917 z"
                />
              </g>
            </svg>
          </a>
          <a className="rss" href="/rss.xml" title="RSS feed">
            <svg
              version="1.1"
              x="0px"
              y="0px"
              viewBox="0 0 25 25"
              width="25"
              height="25"
            >
              <path d="M 0,0 V 25 H 25 V 0 Z m 7.77115,20.46082 c -0.42066,0.41704 -0.99159,0.67138 -1.62714,0.67138 -0.63489,0 -1.20577,-0.25528 -1.62357,-0.67138 -0.41709,-0.41681 -0.67429,-0.98489 -0.67429,-1.61681 0,-0.63198 0.2572,-1.20192 0.67429,-1.61994 0.41874,-0.41588 0.98868,-0.67237 1.62357,-0.67237 0.63555,0 1.20648,0.25649 1.62522,0.6733 0.41874,0.41874 0.67429,0.98703 0.67621,1.61901 -0.002,0.63192 -0.25747,1.19901 -0.67429,1.61681 z m 4.17764,0.69303 c 0,0 0,-0.006 -9.3e-4,-0.006 C 11.94386,18.98246 11.1,16.93922 9.57308,15.41422 8.04637,13.88466 6.00577,13.03752 3.84808,13.03559 V 9.73125 c 3.14829,0.002 5.99829,1.28171 8.06774,3.35242 2.06874,2.07044 3.34858,4.92115 3.35143,8.06973 z m 5.89231,0 C 17.8351,13.43434 11.56659,7.15527 3.85363,7.14951 V 3.84615 c 4.77016,0.002 9.08967,1.94281 12.225,5.07814 3.1334,3.13533 5.07329,7.4584 5.07522,12.22956 z" />
            </svg>
          </a>
        </div>
      </div>
    </header>
  );
};
