import React from 'react';
import Header from './header.js';
import Footer from './footer.js';
import '../css/layout.css';

export default ({ children, title }) => {
  return (
    <div className="wrapper">
      <Header title={title ? title : 'Blog'} />
      <div className="container">{children}</div>
      <Footer />
    </div>
  );
};
