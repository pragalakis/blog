import React from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';

function compare(a, b) {
  if (a.totalCount > b.totalCount) return -1;
  if (a.totalCount < b.totalCount) return 1;
  return 0;
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        allMarkdownRemark(limit: 1000) {
          group(field: frontmatter___tags) {
            fieldValue
            totalCount
          }
        }
      }
    `}
    render={data => {
      // sort tags by totalCount & keep 5 tags
      data = data.allMarkdownRemark.group.sort(compare).slice(0, 5);
      return (
        <div className="row">
          <h3>Popular Tags</h3>
          <ul>
            {data.map(tag => (
              <li key={tag.fieldValue}>
                <Link to={`/tags/${tag.fieldValue}/`}>
                  {tag.fieldValue} ({tag.totalCount})
                </Link>
              </li>
            ))}
          </ul>
        </div>
      );
    }}
  />
);
