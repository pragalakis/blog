import React, { useState } from 'react';

export default () => {
  const [search, setSearch] = useState('');

  function duckduckgo(e) {
    e.preventDefault();
    const win = window.open(
      `https://www.duckduckgo.com/?q=${search}+site:${window.location.hostname}`,
      '_blank'
    );
    win.focus();
  }
  return (
    <div className="row">
      <form className="search" onSubmit={duckduckgo}>
        <label htmlFor="search">Search</label>
        <div className="search-button-container">
          <input
            id="search"
            value={search}
            onChange={e => setSearch(e.target.value)}
            placeholder="Search..."
            type="text"
            name="search"
            required
          />
          <button type="submit">↵</button>
        </div>
      </form>
    </div>
  );
};
