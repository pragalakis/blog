import React from 'react';

export default props => {
  return (
    <div style={{ borderTop: '1px solid #555', padding: '1.5rem 0' }}>
      Share on:{' '}
      <a
        href={`https://twitter.com/intent/tweet?text=${props.title}!&url=${props.location}`}
        target="new"
      >
        Twitter
      </a>
      ,{' '}
      <a
        href={`https://www.facebook.com/sharer/sharer.php?u=${props.location}`}
        target="new"
      >
        Facebook
      </a>
    </div>
  );
};
