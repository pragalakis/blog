import React from 'react';
import Search from './search';
import PopularTags from './popular-tags';
import image from '../images/placeholder-image.png';

export default () => {
  return (
    <aside className="sidebar">
      <Search />
      <PopularTags />
      <div className="row">
        <img src={image} width="100%" height="100%" alt="dummy img" />
      </div>
      <div className="row">
        <img src={image} width="100%" height="100%" alt="dummy img" />
      </div>
      <div className="row">
        <img src={image} width="100%" height="100%" alt="dummy img" />
      </div>
    </aside>
  );
};
