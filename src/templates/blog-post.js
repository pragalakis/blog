import React from 'react';
import { Link, graphql } from 'gatsby';
import Img from 'gatsby-image';
import Layout from '../components/layout';
import Head from '../components/head';
import Share from '../components/share';
import '../css/blog-post.css';

export default (props) => {
  const post = props.data.markdownRemark;
  const siteTitle = props.data.site.siteMetadata.title;
  const { previous, next } = props.pageContext;

  return (
    <Layout title={siteTitle}>
      <Head
        title={post.frontmatter.title}
        description={post.frontmatter.description || post.excerpt}
      />
      <main className="blog-post">
        <h1>{post.frontmatter.title}</h1>
        <div className="date">{post.frontmatter.date}</div>
        <div className="tags">
          {post.frontmatter.tags.map((tag, i) => (
            <Link key={tag + i} to={`/tags/${tag}`}>
              {tag}
            </Link>
          ))}
        </div>
        {post.frontmatter.image === null ? (
          ''
        ) : (
          <Img
            fluid={post.frontmatter.image.childImageSharp.fluid}
            alt="featured-image"
          />
        )}

        <div dangerouslySetInnerHTML={{ __html: post.html }} />

        <Share
          location={props.location.href}
          title={props.data.markdownRemark.frontmatter.title}
        />

        <ul className="blog-post-pagination">
          <li className="previous">
            {previous && (
              <Link to={previous.fields.slug} rel="prev">
                ← {previous.frontmatter.title}
              </Link>
            )}
          </li>
          <li className="next">
            {next && (
              <Link to={next.fields.slug} rel="next">
                {next.frontmatter.title} →
              </Link>
            )}
          </li>
        </ul>
      </main>
    </Layout>
  );
};

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
        tags
        image {
          childImageSharp {
            fluid(maxWidth: 790) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`;
