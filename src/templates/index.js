import React from 'react';
import { Link, graphql } from 'gatsby';
import Layout from '../components/layout';
import Head from '../components/head';
import PostList from '../components/PostList';
import Sidebar from '../components/sidebar';
import paginationRange from '../components/pagination-range';
import '../css/index.css';

export default (props) => {
  const siteTitle = props.data.site.siteMetadata.title;
  const posts = props.data.allMarkdownRemark.edges;
  const { currentPage, numPages } = props.pageContext;

  return (
    <Layout title={siteTitle}>
      <Head title="Home" keywords={[`blog`, `gatsby`, `javascript`, `react`]} />
      <main>
        <h1 className="visually_hidden">Homepage</h1>
        <PostList posts={posts} />
        <div className="pagination">
          {paginationRange(currentPage, numPages).map((v) => {
            return v === '...' ? (
              '...'
            ) : (
              <Link
                className={currentPage === v ? 'active page' : 'page'}
                key={`pagination-number${v}`}
                to={`/${v === 1 ? '' : v}`}
              >
                {v}
              </Link>
            );
          })}
        </div>
      </main>
      <Sidebar />
    </Layout>
  );
};

export const pageQuery = graphql`
  query HomePageQuery($skip: Int!, $limit: Int!) {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
            image {
              childImageSharp {
                fluid(maxWidth: 790) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`;
