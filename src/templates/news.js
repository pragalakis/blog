import React from 'react';
import { Link, graphql } from 'gatsby';
import Head from '../components/head';
import Layout from '../components/layout';
import paginationRange from '../components/pagination-range';
import PostList from '../components/PostList';

export default props => {
  const posts = props.data.allMarkdownRemark.edges;
  const { currentPage, numNewsPages } = props.pageContext;

  return (
    <Layout>
      <Head title="News" />
      <main>
        <h1>News</h1>
        <PostList posts={posts} />
        <div className="pagination">
          {paginationRange(currentPage, numNewsPages).map(v => {
            return v === '...' ? (
              '...'
            ) : (
              <Link
                className={currentPage === v ? 'active page' : 'page'}
                key={`pagination-number${v}`}
                to={`/news/${v === 1 ? '' : v}`}
              >
                {v}
              </Link>
            );
          })}
        </div>
      </main>
    </Layout>
  );
};

export const pageQuery = graphql`
  query($skip: Int!, $limit: Int!) {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: ["news"] } } }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMM DD, YYYY")
            title
            description
            image {
              childImageSharp {
                fluid(maxWidth: 790) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`;
