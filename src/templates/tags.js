import React from 'react';
import { Link, graphql } from 'gatsby';
import Head from '../components/head';
import Layout from '../components/layout';
import PostList from '../components/PostList';

export default ({ pageContext, data }) => {
  const { tag } = pageContext;
  const { edges, totalCount } = data.allMarkdownRemark;
  const tagHeader = `${totalCount} post${
    totalCount === 1 ? '' : 's'
  } tagged with "${tag}"`;

  return (
    <Layout>
      <Head title={tag} />
      <main>
        <h1>{tagHeader}</h1>
        <PostList posts={edges} />
        <div className="all-tags">
          <Link to="/tags">All tags</Link>
        </div>
      </main>
    </Layout>
  );
};

export const pageQuery = graphql`
  query($tag: String) {
    allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMM DD, YYYY")
            title
            description
          }
        }
      }
    }
  }
`;
